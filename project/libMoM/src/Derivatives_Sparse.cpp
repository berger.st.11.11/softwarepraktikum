#define DCO_P1F_SIZE 128
#include "Function.hpp"
namespace MoM {
template <typename T, typename VT>
Derivatives_Sparse<T, VT>::Derivatives_Sparse(const int order, const VT &mean)
    : Derivatives<T, VT>(order, mean) {}

template <typename T, typename VT>
void Derivatives_Sparse<T, VT>::fill_tensor_lin() {
  vector<T> x(this->_num_var, 1.0);  //
  std::vector<std::vector<bool>> v_pH(this->_num_var,
                                      std::vector<bool>(this->_num_var));
  pattern_hessian(x, v_pH);

  // Convert to data structure required by ColPack
  unsigned int **pH = new unsigned int *[this->_num_var]();
  for (int i = 0; i < this->_num_var; i++)  // zeile
  {
    int nnz = 0;
    for (int j = 0; j < this->_num_var; j++)  // spalte
    {
      if (v_pH[i][j] || i == j) {
        nnz++;
      }
    }
    pH[i] = new unsigned int[nnz + 1]();
    pH[i][0] = nnz;
    nnz = 1;
    for (int j = 0; j < this->_num_var; j++) {
      if (v_pH[i][j] || i == j) {
        pH[i][nnz++] = j;
      }
    }
  }
  // Coloring
  int color_dummy, n_colors;
  ColPack::GraphColoringInterface *G =
      new ColPack::GraphColoringInterface(SRC_MEM_ADOLC, pH, x.size());
  G->Coloring("SMALLEST_LAST", "STAR");
  double **seed = G->GetSeedMatrix(&color_dummy, &n_colors);
//Für Ordnung 2  
if(this->_order==2)
{
  std::vector<double> x2(
      this->_num_var,
      0.0);  // nur zum initialisieren der Hessematrix und Jacobimatrix
  std::vector<double> J(x2);  // Jacobimatrix
  std::vector<std::vector<double>> H_compressed(this->_num_var,
                                                x2);       // komprimierte Hesse
  std::vector<std::vector<double>> H(this->_num_var, x2);  // Hesse

  // Verschachtelung dco-Typen:
  typedef dco::gt1s<double>::type t1s_type;
  typedef dco::gt1s<t1s_type>::type t2s_t1s_type;
  // Deklaration x und y:
  std::vector<t2s_t1s_type> t2s_t1s_x(this->_num_var);
  t2s_t1s_type t2s_t1s_y;

  // initialization of values, first and second directional
  // derivatives of active inputs
  for (int l = 0; l < n_colors; l++) {
    for (int k = 0; k < this->_num_var; k++) {
      for (int i = 0; i < this->_num_var; i++) {
        t1s_type v = dco::value(t2s_t1s_x[i]);
        dco::value(v) = this->_mean[i];
        if (k == i) {
          dco::derivative(v) = 1;
        } else {
          dco::derivative(v) = 0;
        }
        dco::value(t2s_t1s_x[i]) = v;
        // bis hierhin erste Ableitung, danach zweite Ableitung (bei zweiter
        // Graphfärbung benutzen)
        // v= dco::derivative(t2s_t1s_x[i]); danke für das gute example
        dco::value(v) = 0;
        if (seed[i][l] > 0) dco::value(v) = 1;
        dco::derivative(v) = 0;
        dco::derivative(t2s_t1s_x[i]) = v;
      }
      // }
      // overloaded function evaluation
      f(t2s_t1s_x, t2s_t1s_y);
      // extraction of values, first and second directional
      // derivatives from active outputs

      t1s_type v;
      v = dco::value(t2s_t1s_y);
      // if (k + l == 0) alt-> nur noch für verständnis da
      //       y = dco::value(v);
      J[k] = dco::derivative(v);  // Jacobimatrix
      v = dco::derivative(t2s_t1s_y);
      H_compressed[l][k] = dco::derivative(v);  // komprimierte Hessematrix
    }
  }
  // Dekomprimieren der Hessematrix
    for (int i = 0; i < n_colors; i++)
    {
      for (int u = 0; u < this->_num_var; u++)
      {

        if (seed[u][i] > 0)
        {
          for (size_t o = 0; o < pH[u][0]; o++)
          {
            H[u][pH[u][o + 1]] = H_compressed[i][pH[u][o + 1]];
            H_compressed[i][pH[u][o + 1]] = 0;
          }
        }
      }
    }
  // Hessematrix in lin_der_tensor reinschreiben:
  // 0-ter Eintrag
  f(this->_mean, this->_lin_der_tensor[0]);

  // Jacobimatrix übertragen:
  for (int i = 0; i < this->_num_var; i++) {
    this->_lin_der_tensor[i + 1] = J[i];
  }
  // Hessematrix übertragen:
  int position = 1 + this->_num_var;
  for (int i = 0; i < this->_num_var; i++) {
    for (int j = 0; j < i + 1; j++) {
      this->_lin_der_tensor[position++] = H[i][j];
    }
  }
}
//Für Ordnung 3
if(this->_order == 3)
{
   std::vector<T> Jacobian(this->_num_var, 0); // Jacobimatrix

    std::vector<std::vector<T>> Hesse_compressed(
        this->_num_var, std::vector<T>(this->_num_var, 0)); // komprimierte Hesse
    std::vector<std::vector<T>> Hesse(
        this->_num_var, std::vector<T>(this->_num_var, 0)); // Hesse

    // Dritte Ableitung
    std::vector<std::vector<std::vector<T>>> Third_Der_compressed(
        this->_num_var, std::vector<std::vector<T>>(
                            this->_num_var, std::vector<T>(this->_num_var, 0)));
    std::vector<std::vector<std::vector<T>>> Third_Der(
        this->_num_var, std::vector<std::vector<T>>(
                            this->_num_var, std::vector<T>(this->_num_var, 0)));
    std::vector<std::vector<std::vector<T>>> Third_Der_less_compressed(
        this->_num_var, std::vector<std::vector<T>>(
                            this->_num_var, std::vector<T>(this->_num_var, 0)));

    // =========Ab hier wird die Ableitung berechnet============================

    // Verschachtelung dco-Typen:
    typedef typename dco::gt1s<T>::type t1s_type;
    typedef typename dco::gt1s<t1s_type>::type t2s_t1s_type;
    typedef typename dco::gt1s<t2s_t1s_type>::type t3s_t2s_t1s_type;

    // Deklaration x und y:
    // std::vector<t2s_t1s_type> t2s_t1s_x(this->_num_var);
    // t2s_t1s_type t2s_t1s_y;

    std::vector<t3s_t2s_t1s_type> t3s_t2s_t1s_x(this->_num_var);
    t3s_t2s_t1s_type t3s_t2s_t1s_y;

    for (int i = 0; i < this->_num_var; ++i)
      dco::value(dco::value(dco::value(t3s_t2s_t1s_x[i]))) = this->_mean[i];

    // Ableitungen berechen
    for (int i = 0; i < this->_num_var; ++i)
    {
      // Erste Ableitung
      dco::derivative(dco::value(dco::value(t3s_t2s_t1s_x[i]))) = 1;
      for (int col = 0; col < n_colors; ++col)
      {
        // In diesen Schleifen sollten alle unabhaengigen Ableitungen auf 1
        // gesetzt werden
        for (int j = 0; j < this->_num_var; ++j)
        {
          // Zweite Ableitung
          if (seed[j][col] > 0)
          {
            dco::value(dco::derivative(dco::value(t3s_t2s_t1s_x[j]))) = 1;
          }
          else
          {
            dco::value(dco::derivative(dco::value(t3s_t2s_t1s_x[j]))) = 0;
          }
        }
          for (int col2 = 0; col2 < n_colors; ++col2)
          {
            for (int k = 0; k < this->_num_var; ++k)
            {
              // Dritte Ableitung
              if (seed[k][col2] > 0)
              {
                dco::value(dco::value(dco::derivative(t3s_t2s_t1s_x[k]))) = 1;
              }
              else
              {
                dco::value(dco::value(dco::derivative(t3s_t2s_t1s_x[k]))) = 0;
              }
            }
            f(t3s_t2s_t1s_x, t3s_t2s_t1s_y);
            Jacobian[i] = dco::derivative(dco::value(dco::value(t3s_t2s_t1s_y)));
            Hesse_compressed[col][i] =
                dco::derivative(dco::derivative(dco::value(t3s_t2s_t1s_y)));
            Third_Der_compressed[col2][col][i] =
                dco::derivative(dco::derivative(dco::derivative(t3s_t2s_t1s_y)));
            // std::cout << Third_Der_compressed[col][i][j] << "\n";
          }
        
      }
      dco::derivative(dco::value(dco::value(t3s_t2s_t1s_x[i]))) = 0;
    }

    // Dekomprimieren der Hessematrix
    for (int i = 0; i < n_colors; i++)
    {
      for (int u = 0; u < this->_num_var; u++)
      {

        if (seed[u][i] > 0)
        {
          for (size_t o = 0; o < pH[u][0]; o++)
          {
            Hesse[u][pH[u][o + 1]] = Hesse_compressed[i][pH[u][o + 1]];
            Hesse_compressed[i][pH[u][o + 1]] = 0;
          }
        }
      }
    }
    //Dekomprimiere dritte Ableitung
    for (int i = 0; i < n_colors; i++)
    {
      bool swap;
      for (int j = 0; j < this->_num_var; j++)
      {
        if (seed[j][i] == 1)
        {
          swap = true;
        }
        else
        {
          swap = false;
        }
        if (swap)
        {
          for (int k = 0; k < pH[j][0]; k++)
          {
            for (int l = 0; l < n_colors; l++)
            {
              Third_Der_less_compressed[j][l][pH[j][k + 1]] = Third_Der_compressed[i][l][pH[j][k + 1]];
              Third_Der_compressed[i][l][pH[j][k + 1]] = 0;
            }
          }
        }
      }
    } 
      //zweiter Teil
      for (int k = 0; k < this->_num_var; k++)
      {
        for (int i = 0; i < n_colors; i++)
        {
          for (int u = 0; u < this->_num_var; u++)
          {
            if (seed[u][i] > 0)
            {
              for (size_t o = 0; o < pH[u][0]; o++)
              {
                Third_Der[k][u][pH[u][o + 1]] = Third_Der_less_compressed[k][i][pH[u][o + 1]];
                Third_Der_less_compressed[k][i][pH[u][o + 1]] = 0;
              }
            }
          }
        }
      }

    // lin_der_tensor fuellen:
    // 0-ter Eintrag
    f(this->_mean, this->_lin_der_tensor[0]);
    // Jacobimatrix übertragen:
    for (int i = 0; i < this->_num_var; i++)
    {
      this->_lin_der_tensor[i + 1] = Jacobian[i];
    }
    // Hessematrix übertragen:
    int position = 1 + this->_num_var;
    for (int i = 0; i < this->_num_var; i++)
    {
      for (int j = 0; j < i + 1; j++)
      {
        this->_lin_der_tensor[position++] = Hesse[i][j];
      }
    }
    // Dritte Ableitung uebertragen (?)
    for (int i = 0; i < this->_num_var; i++)
    {
      for (int j = 0; j < i + 1; j++)
      {
        for (int k = 0; k < j + 1; k++)
        {
          this->_lin_der_tensor[position++] = Third_Der[i][j][k];
        }
      }
    }
}
}

template <typename T, typename VT>
template <typename S>
void Derivatives_Sparse<T, VT>::adjoint_driver(const std::vector<S> &x, S &y,
                                               std::vector<S> &g) {
  typedef typename dco::ga1sm<S> A1S_MODE;
  typedef typename A1S_MODE::type ADTYPE;
  typedef typename A1S_MODE::tape_t TAPE_TYPE;
  TAPE_TYPE *tape = TAPE_TYPE::create();
  std::vector<ADTYPE> ad_x(this->_num_var);
  ADTYPE ad_y;
  for (int i = 0; i < this->_num_var; i++) {
    ad_x[i] = x[i];
    tape->register_variable(ad_x[i]);
  }
  f(ad_x, ad_y);
  dco::derivative(ad_y) = 1.0;
  tape->interpret_adjoint();
  for (int i = 0; i < this->_num_var; i++) {
    g[i] = dco::derivative(ad_x[i]);
  }
  y = dco::value(ad_y);
  TAPE_TYPE::remove(tape);
}

template <typename T, typename VT>
void Derivatives_Sparse<T, VT>::pattern_hessian(
    const std::vector<T> &x, std::vector<std::vector<bool>> &pH) {
  std::vector<dco::p1f::type> p1f_x(x.size());
  dco::p1f::type p1f_y;
  std::vector<dco::p1f::type> p1f_g(x.size());
  for (size_t i = 0; i < x.size(); i++) {
    p1f_x[i] = x[i];
    dco::p1f::set(p1f_x[i], true, i);
  }
  adjoint_driver(p1f_x, p1f_y, p1f_g);
  bool dep;
  for (int i = 0; i < this->_num_var; i++) {
    for (int j = 0; j < this->_num_var; j++) {
      dco::p1f::get(p1f_g[i], dep, j);
      if (dep)
        pH[i][j] = true;
      else
        pH[i][j] = false;
    }
  }
}

}  // namespace MoM