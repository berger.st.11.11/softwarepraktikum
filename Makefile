all:
	cd project && $(MAKE)

doc:
	cd project && $(MAKE) doc

test:
	cd project && $(MAKE) test

example:
	cd project && $(MAKE) example

clean:
	cd project && $(MAKE) clean

.PHONY: all doc clean