# SoftwarePraktikum

# Literatur
* Momenten-Methoden für die ersten beiden Momente (mean, variance):
    - Bruce Christianson, Maurice Cox: Automatic Propagation of Uncertainties
    - Putko, Newman, Taylor, Green: Approach for Uncertainty Propagation and Robust Design in CFD Using Sensitivity Derivatives

* Algorithmischem Differenzieren:
    - Naumann: The Art of Differentiating ComputerPrograms: An Introduction to Algorithmic Differentiation
    - dco als AD Software wird in der Veranstalltung eingeführt.

* Färbungsalgorithmen zum Berechnen dünnbesetzter Ableitungen:
    - Assefaw Hadish Gebremedhin, Fredrik Manne, Alex Pothen: What Color is your Jacobian?
    - Jens Deussen, Uwe Naumann: Efficient Computation of Sparse Higher Derivative Tensors
