#include <Eigen/Dense>
#include <chrono>
#include <ctime>
#include <iostream>
#include <random>

#include "MoM.hpp"

// Funktion fuer die die Momentenmethode ausgefuehrt wird
template <typename T, typename VT>
void MoM::f(const VT& x, T& y) {
  y = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    y += x[i] * x[i] * x[i];
  }
}

int main(int argc, char* argv[]) {
  // Setup
  int num_var = 50, order = 3;
  if (argc >= 3) {
    num_var = std::stoi(argv[1]);
    order = std::stoi(argv[2]);
  }
  using T = double;
  // using VT = Eigen::Matrix<double, Eigen::Dynamic, 1>;
  using VT = std::vector<T>;

  // VT mean_input = VT::Random(num_var, 1) * 2.75;
  // VT std_dev = VT::Random(num_var, 1) * 1.5;
  VT mean_input = VT(num_var, 1);
  VT std_dev = VT(num_var, 0.01);

  std::cout << "\033[31mSparse Function:\n\033[0m";
  std::cout << "num_var = " << num_var << " | order = " << order << "\n\n";

  try {
    int n = 1000000;
    std::vector<VT> x(n);
    std::vector<T> y(n);

    for (int j = 0; j < num_var; j++) {
      srand((unsigned)time(0));
      std::default_random_engine generator;
      generator.seed(std::random_device{}());
      std::normal_distribution<T> distribution(mean_input[j], std_dev[j]);
      for (int i = 0; i < n; i++) {
        x[i].push_back(distribution(generator));
      }
    }

    // for (int i = 0; i < 15; ++i) {
    //   for (auto &entry : x[i]) std::cout << entry << " ";
    //   std::cout << "\n";
    // }

    T sum = 0;
    T runs = n;
    for (int i = 0; i < n; i++) {
      MoM::f(x[i], y[i]);
      sum += y[i];
    }
    T mean = (sum / runs);

    T var = 0;
    for (int i = 0; i < n; i++) {
      // MoM::f(x[i], y[i]);
      var = var + pow(mean - y[i], 2);
    }
    var = var / runs;

    std::cout << "mean_monte_carlo     = " << mean << std::endl;
    std::cout << "variance_monte_carlo = " << var << std::endl;
    std::cout << std::endl;
  } catch (std::exception& e) {
    std::cerr << "Error in Monte Carlo: " << e.what() << "\n";
  }

  // Dense
  try {
    auto t1 = std::chrono::high_resolution_clock::now();

    MoM::Derivatives_Dense<T, VT>* derd =
        new MoM::Derivatives_Dense<T, VT>(order, mean_input);
    MoM::MethodOfMoments<T, VT>* momd =
        new MoM::MethodOfMoments<T, VT>(std_dev, *derd);
    momd->calc_mean();
    momd->calc_variance();

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << "Time MoM Dense: \033[31m" << duration / 1E6 << "s\033[0m"
              << std::endl;

    std::cout << "F_mean_dense      = " << momd->mean_output() << std::endl;
    std::cout << "F_variance_dense  = " << momd->variance_output() << std::endl;
    std::cout << std::endl;

    delete derd, momd;
  } catch (std::exception& e) {
    std::cerr << "Error in Dense: " << e.what() << "\n";
  }

  // Sparse
  try {
    auto t1 = std::chrono::high_resolution_clock::now();

    MoM::Derivatives_Sparse<T, VT>* ders =
        new MoM::Derivatives_Sparse<T, VT>(order, mean_input);
    MoM::MethodOfMoments<T, VT>* moms =
        new MoM::MethodOfMoments<T, VT>(std_dev, *ders);

    moms->calc_mean();
    moms->calc_variance();

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();
    std::cout << "Time MoM Sparse: \033[31m" << duration / 1E6 << "s\033[0m"
              << std::endl;

    std::cout << "F_mean_sparse     = " << moms->mean_output() << std::endl;
    std::cout << "F_variance_sparse = " << moms->variance_output() << std::endl;
    std::cout << std::endl;

    delete ders, moms;
  } catch (std::exception& e) {
    std::cerr << "Error in Sparse: " << e.what() << "\n";
  }

  return 0;
}
