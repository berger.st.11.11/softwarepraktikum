#pragma once

#include <cmath>

namespace MoM {

/**
  Deklaration der Funktion, fuer welche die Momentenmethode bestimmt wird
  -> muss vom Nutzer definiert werden
*/
template <typename T, typename VT>
void f(const VT &x, T &y);

}  // namespace MoM