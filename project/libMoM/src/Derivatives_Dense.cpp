#define MAXIMALE_ORDNUNG 10

namespace MoM {

// Klasse fuer die rekursive Berechnung der Ableitung
template <unsigned int n>
template <typename T, typename VT>
void deriv<n>::dern(const VT &x, T &dy, std::vector<int> direction) {
  using DCO_T = typename dco::gt1s<T>::type;

  int num_var = direction.size();

  DCO_T _y;
  std::vector<DCO_T> _x(num_var);

  for (int i = 0; i < num_var; i++) {
    dco::value(_x[i]) = x[i];
  }

  // Richtung richtig setzen

  for (int i = 0; i < num_var; i++) {
    if (direction[i] >= 1) {
      dco::derivative(_x[i]) = 1;
      direction[i] -= 1;
      deriv<n - 1>::dern(_x, _y, direction);
      break;  // andere Mögliuchkeit: direction static machen
    }
  }
  // Vielleicht fix für constexpr Problem
  int a = 0;
  for (int i = 0; i < num_var; i++) {
    a += direction[i];
  }
  if (a == 0) {
    // f(_x, _y);
    f(_x, _y);
    // Erlaubt Angabe falscher Ordnung (trotzdem richtiges
    // Ergebnis, da direction = 0 <=> Ordnung = 0)
  }
  dy = dco::derivative(_y);
}

// Basisfall
template <typename T, typename VT>
void deriv<0>::dern(const VT &x, T &dy, std::vector<int>) {
  // direction wird nicht genutzt
  f(x, dy);
}

// Bestimmt Partitionen (?)
template <typename T, typename VT>
void Derivatives_Dense<T, VT>::partitions(int sum, int k) {
  if (k == 1) {
    parts[k - 1] = sum;
    v.push_back(parts);
    return;
  }
  for (int i = 0; i <= sum; i++) {
    parts[k - 1] = i;
    partitions(sum - i, k - 1);
  }
}

// Bestimmt die moeglichen Ableitungen fuer entsprechende Ordnung
template <typename T, typename VT>
void Derivatives_Dense<T, VT>::direction_calculate(
    const int order, std::vector<std::vector<int>> &permutations) {
  permutations.clear();
  v.clear();
  parts.clear();
  std::vector<int> placeholder = std::vector<int>(this->_num_var, 0);
  int sum = order;
  for (int i = 0; i < this->_num_var; i++) parts.push_back(0);
  partitions(sum, this->_num_var);

  for (unsigned int i = 0; i < v.size(); i++) {
    for (unsigned int j = 0; j < v[i].size(); j++) {
      placeholder[j] = v[i][j];
    }
    permutations.push_back(placeholder);
  }
}

// Konstruktor
template <typename T, typename VT>
Derivatives_Dense<T, VT>::Derivatives_Dense(const int order, const VT &mean)
    : Derivatives<T, VT>(order, mean) {}

// Befuellt den linearisierten Ableitungstensor
template <typename T, typename VT>
void Derivatives_Dense<T, VT>::fill_tensor_lin() {
  std::vector<std::vector<int>> permutations;

  // wert fuer 0 (0-te Ableitung)
  direction_calculate(0, permutations);
  deriv<0>::dern<T, VT>(this->_mean, this->_lin_der_tensor[0], permutations[0]);
  // Hier maximale mögliche Ordnung festlegen (kann auch über maximaler
  // akuteller Ordnung sein)
  constexpr unsigned int maximaleOrdnung = MAXIMALE_ORDNUNG;

  int position_lin = 0;  // size X
  /* #pragma omp paralel for private(position_lin) schedule(dynamic, 1) \
      num_threads(4) */
  for (int i = 1; i < this->_order + 1; ++i) {
    direction_calculate(i, permutations);

    for (auto it = std::begin(permutations); it < std::end(permutations);
         ++it) {
      ++position_lin;
      deriv<maximaleOrdnung>::dern<T, VT>(
          this->_mean, this->_lin_der_tensor[position_lin], *it);
    }
  }
}

}  // namespace MoM
