#pragma once

#include <vector>

#include "Derivatives.hpp"

namespace MoM {

template <typename T, typename VT>
class MethodOfMoments {
  /// Order
  const int _order;
  /// standard deviation used to calculate mean and std_deviation
  VT _std_deviation;
  /// derivatives at mean value
  std::vector<T> _derivatives;
  /// Output Moments
  T _mean_output, _variance_output;

  /// wichtig fuer direction bestimmung
  std::vector<std::vector<int>> v;
  std::vector<int> parts;
  /// directions
  void partitions(int, int);
  void direction_calculate(const int, const int,
                           std::vector<std::vector<int>>&);

 public:
  /// Konstruktoren -> Bekommt std_deviation & derivatives in bekanntem Format
  MethodOfMoments(const VT&, Derivatives<T, VT>&);

  /// Berechnet Varianz
  void calc_variance();
  /// Berechnet Erwartungswert
  void calc_mean();
  /// Gibt uebergebene std_deviation zurueck
  VT& std_deviation();
  /// Gibt uebergebene derivatives zurueck
  std::vector<T>& derivatives();
  /// Gibt Erwartungswert zurueck
  T& mean_output();
  /// Gibt Varianz zurueck
  T& variance_output();
};

}  // namespace MoM

#include "../src/MethodOfMoments.cpp"