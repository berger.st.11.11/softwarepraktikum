#include <cmath>

namespace MoM {

/// Konstruktor
template <typename T, typename VT>
MethodOfMoments<T, VT>::MethodOfMoments(const VT &std_deviation,
                                        Derivatives<T, VT> &der)
    : _order(der.order()), _std_deviation(std_deviation) {
  der.fill_tensor_lin();
  _derivatives = der.lin_der_tensor();
}

/// Berechnet Varianz
template <typename T, typename VT>
void MethodOfMoments<T, VT>::calc_variance() {
  // Anzahl der freien Variablen
  const int n = _std_deviation.size();

  /*
    Nur von intersse, wenn mind. erste Ableitung und in Summe kleiner
    gleich der Ordnung -> position_lin beginnt bei 1 & permutationen auch
    bei Ordnung 1 => ((sum_indices <= _order) && (sum_indices > 0)) == true
  */
  int position_lin = 1;
  std::vector<std::vector<int>> permutations;

  _variance_output = 0;  // initialisiere _variance_output auf Null
  for (int i = 1; i < _order + 1; ++i) {
    direction_calculate(n, i, permutations);
    for (unsigned int j = 0; j < permutations.size(); ++j) {
      std::vector<int> indices = permutations[j];
      int fun = 0;
      // Produkt der Standardabweichungen hoch dem entsprechendem Index
      T product_std_deviation = 1;
      for (int k = 0; k < n; ++k) {
        product_std_deviation *= std::pow(_std_deviation[k], indices[k]);
      }
      for (int p = 0; p < n; p++) {
        if (indices[p] % 2 == 0) {
          fun++;
        }
      }

      /*
        Entsteht aus Multinominalkoeffizient (wegen der symmetrie der
        Ableitungen) und dem Teilen durch (sum_indices)!
        =>  (sum_indizes!)/(index1! * index2! * ... * indexN! * sum_indices!)
             = 1/(index1! * ... * indexN!)
      */
      T indices_factorial = 1;
      for (int l = 0; l < indices.size(); ++l)
        indices_factorial /= tgamma(indices[l] + 1);

      // tgamma(n+1) = n! -> Gamma function
      // if(fun != n)
      _variance_output +=
          indices_factorial *
          pow(product_std_deviation * _derivatives[position_lin], 2);
      ++position_lin;
    }
  }
}

/// Berechnet Erwartungswert
template <typename T, typename VT>
void MethodOfMoments<T, VT>::calc_mean() {
  // Anzahl der freien Variablen
  const int n = _std_deviation.size();

  /*
    Setzt korrekten Startwert, da wir erst ab zweiter Ableitung anfangen
    Nur von intersse, wenn mind. zweite Ableitung und in Summe kleiner
    gleich der Ordnung -> permutations ab 2 & position_lin wird angepasst
    => (sum_indices <= _order) && (sum_indices > 1) == true
  */
  int position_lin = 1 + n;
  std::vector<std::vector<int>> permutations;

  _mean_output = _derivatives[0];  // _mean_output auf F(b_mean) initialisiert

  for (int i = 2; i < _order + 1; i++) {
    direction_calculate(n, i, permutations);
    for (unsigned int j = 0; j < permutations.size(); ++j) {
      // Bestimmt die Indizes, ueberfuhrt von 1D nach nD
      std::vector<int> indices = permutations[j];

      // Produkt der Standardabweichungen hoch dem entsprechendem Index
      T product_std_deviation = 1;
      int check_if_needed = 0;
      for (int k = 0; k < n; ++k) {
        product_std_deviation *= std::pow(_std_deviation[k], indices[k]);
      }
      for (int p = 0; p < n; ++p) {
        if (indices[p] % 2 == 0) {
          ++check_if_needed;
        }
      }
      /*
        Entsteht aus Multinominalkoeffizient (wegen der symmetrie der
        Ableitungen) und dem Teilen durch (sum_indices)!
        =>  (sum_indizes!)/(index1! * index2! * ... * indexN! * sum_indices!)
            = 1/(index1! * ... * indexN!)
      */
      T indices_factorial = 1;
      for (int l = 0; l < indices.size(); ++l)
        indices_factorial /= tgamma(indices[l] + 1);

      // tgamma(n+1) = n! -> Gamma function
      if (check_if_needed == n) {
        _mean_output += indices_factorial * product_std_deviation *
                        _derivatives[position_lin];
      }
      ++position_lin;
    }
  }
}

/// Partitionen
template <typename T, typename VT>
void MethodOfMoments<T, VT>::partitions(int sum, int k) {
  if (k == 1) {
    parts[k - 1] = sum;
    v.push_back(parts);
    return;
  }
  for (int i = 0; i <= sum; i++) {
    parts[k - 1] = i;
    partitions(sum - i, k - 1);
  }
}

/// Permutationen
template <typename T, typename VT>
void MethodOfMoments<T, VT>::direction_calculate(
    const int num_var, const int order,
    std::vector<std::vector<int>> &permutations) {
  permutations.clear();
  v.clear();
  parts.clear();
  std::vector<int> placeholder = std::vector<int>(num_var, 0);
  int k = num_var;
  int sum = order;
  for (int i = 0; i < k; i++) parts.push_back(0);
  partitions(sum, k);

  for (unsigned int i = 0; i < v.size(); i++) {
    for (unsigned int j = 0; j < v[i].size(); j++) {
      placeholder[j] = v[i][j];
    }
    permutations.push_back(placeholder);
  }
}

/// Zugriff auf std_deviation
template <typename T, typename VT>
VT &MethodOfMoments<T, VT>::std_deviation() {
  return _std_deviation;
}

/// Zugriff auf derivatives
template <typename T, typename VT>
std::vector<T> &MethodOfMoments<T, VT>::derivatives() {
  return _derivatives;
}

/// Zugriff auf mean
template <typename T, typename VT>
T &MethodOfMoments<T, VT>::mean_output() {
  return _mean_output;
}

/// Zugriff auf variance
template <typename T, typename VT>
T &MethodOfMoments<T, VT>::variance_output() {
  return _variance_output;
}

}  // namespace MoM