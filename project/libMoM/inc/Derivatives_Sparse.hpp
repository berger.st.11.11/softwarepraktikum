#pragma once

#include <iostream>
#include <vector>

#include "ColPackHeaders.h"
#include "Derivatives.hpp"
#include "Eigen/Dense"
#include "Function.hpp"
#include "dco.hpp"

#define DCO_P1F_SIZE 128

namespace MoM {

template <typename T, typename VT>
class Derivatives_Sparse : public Derivatives<T, VT> {
 protected:
 ///adjoint für sparsity pattern
  template <typename S>
  void adjoint_driver(const std::vector<S> &x, S &y, std::vector<S> &g);
///berechnet sparsity pattern
  void pattern_hessian(const std::vector<T> &x,
                       std::vector<std::vector<bool>> &pH);

 public:
 ///Konstruktor
  Derivatives_Sparse(const int, const VT &);
///Tensor wird aufgefuellt (sparse)
  void fill_tensor_lin();
};
}  // namespace MoM

#include "../src/Derivatives_Sparse.cpp"
