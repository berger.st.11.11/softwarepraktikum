#pragma once
#include <vector>

namespace MoM {

template <typename T, typename VT>
class Derivatives {
 protected:
  int _order, _num_var;
  VT _mean;
  std::vector<T> _lin_der_tensor;

  int num_permutations(int, int);

 public:
  // Konstruktor
  Derivatives(const int, const VT &);

  // Gibt den linearisierten Ableitungstensor zurueck
  const std::vector<T> &lin_der_tensor();

  // Berechnet die Ableitungen
  virtual void fill_tensor_lin() = 0;

  // Gibt Ordnung zurueck
  const int &order();
};

}  // namespace MoM

#include "../src/Derivatives.cpp"
