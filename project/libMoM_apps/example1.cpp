#include <Eigen/Dense>
#include <chrono>
#include <ctime>
#include <iostream>
#include <random>

#include "MoM.hpp"

// Funktion fuer die die Momentenmethode ausgefuehrt wird
template <typename T, typename VT>
void MoM::f(const VT& x, T& y) {
  y = x[0] + x[1] + x[2];
}

int main() {
  const int num_var = 3, order = 1;

  using T = double;
  using VT = std::vector<T>;

  VT mean_input = {40.0, 60.0, 16.67};
  VT std_dev = {37.616, 34.641, 13.229};

  MoM::Derivatives_Dense<T, VT> derivatives(order, mean_input);
  MoM::MethodOfMoments<T, VT> mom(std_dev, derivatives);

  mom.calc_mean();
  mom.calc_variance();

  std::cout << "\033[31mF(x) = x[0] + x[1] + x[2]\033[0m\n\n";
  std::cout << "x_mean    = [" << mean_input[0] << ", " << mean_input[1] << ", "
            << mean_input[2] << "]\n";
  std::cout << "x_std_dev = [" << std_dev[0] << ", " << std_dev[1] << ", "
            << std_dev[2] << "]\n\n";
  std::cout << "order = " << order << "\n\n";
  std::cout << "\033[31m";
  std::cout << "F_mean     = " << mom.mean_output() << "\n";
  std::cout << "F_variance = " << mom.variance_output() << "\n";
  std::cout << "\033[0m";

  return 0;
}