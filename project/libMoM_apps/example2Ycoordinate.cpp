#include <math.h>

#include <Eigen/Dense>
#include <chrono>
#include <fstream>
#include <iostream>

#include "MoM.hpp"

// Funktion fuer die die Momentenmethode ausgefuehrt wird
template <typename T, typename VT>
<<<<<<< HEAD
void MoM::f(const VT &x, T &y)
{
    //Masse                 0
    //Querschnitt           1
    //Luftwiderstand        2
    //Luftdichte            3
    //abwurfgeschwindigkeit 4
    //abwurfwinkel          5
    //zeitpunkt             6

    T g = 9.81;
    T v0_x = x[4] * cos(x[5] * 3.14159265 / 180);
    T v0_y = x[4] * sin(x[5] * 3.14159265 / 180);

    T k = 0.5 * x[3] * x[2] * x[1];
    T v_inf = pow((x[0] * g / k), 0.5);
    T t_u = v_inf / g * atan(v0_y / v_inf);

    if (x[6] <= t_u)
    {
        y = (pow(v_inf,2)/g *(log(cos(g*(t_u-x[6])/v_inf)) - log(cos(g*t_u/v_inf))));
    }
    else
    {
        y = (pow(v_inf ,2) / g * (-g * (x[6] - t_u) / v_inf - log((1 + exp(-2 * g * (x[6] - t_u) / v_inf)) / 2 * cos(g * t_u / v_inf))));
    }
=======
void MoM::f(const VT &x, T &y) {
  // Masse                 0
  // Querschnitt           1
  // Luftwiderstand        2
  // Luftdichte            3
  // abwurfgeschwindigkeit 4
  // abwurfwinkel          5
  // zweitpunkt            6

  T g = 9.81;
  T v0_x = x[4] * cos(x[5] * 3.14159265 / 180);
  T v0_y = x[4] * sin(x[5] * 3.14159265 / 180);

  T k = 0.5 * x[3] * x[2] * x[1];
  T v_inf = pow((x[0] * g / k), 0.5);
  T t_u = v_inf / g * atan(v0_y / v_inf);

  if (x[6] <= t_u) {
    y = (pow(v_inf, 2) / g *
         (log(cos(g * (t_u - x[6]) / v_inf)) - log(cos(g * t_u / v_inf))));
  } else {
    y = (pow(v_inf, 2) / g *
         (-g * (x[6] - t_u) / v_inf -
          log((1 + exp(-2 * g * (x[6] - t_u) / v_inf)) / 2 *
              cos(g * t_u / v_inf))));
  }
>>>>>>> cbac77704926ee9724410370ffe21fbfc87d0cfd
}

int main(int argc, char *argv[]) {
  // Setup
  const int order = 4;
  const int num_var = 7;
  using T = double;
  using VT = std::vector<T>;

  // Dense

  try {
    std::ofstream myfile;
    myfile.open("example2y.csv");

    // generate timepoints
    int points = 200;
    double maxtime = 20;
    double time_points[points];
    for (int i = 0; i < points; i++) {
      time_points[i] =
          (static_cast<double>(i) * maxtime / static_cast<double>(points));
    }

    for (int i = 0; i < points; i++) {
      VT mean_input = {1, 0.045, 0.45, 1.25, 100, 30, time_points[i]};
      VT std_dev = {0.0001, 0.000001, 0.005, 0.001, 6, 3, 0};

      auto t1 = std::chrono::high_resolution_clock::now();

      MoM::Derivatives_Dense<T, VT> *derd =
          new MoM::Derivatives_Dense<T, VT>(order, mean_input);
      MoM::MethodOfMoments<T, VT> *momd =
          new MoM::MethodOfMoments<T, VT>(std_dev, *derd);
      momd->calc_mean();
      momd->calc_variance();

      auto t2 = std::chrono::high_resolution_clock::now();
      auto duration =
          std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1)
              .count();
      std::cout << "Time MoM Dense: \033[31m" << duration / 1E6 << "s\033[0m"
                << std::endl;

      std::cout << "F_mean_dense      = " << momd->mean_output() << std::endl;
      std::cout << "F_variance_dense  = " << momd->variance_output()
                << std::endl;
      std::cout << std::endl;
      myfile << i << "," << time_points[i] << "," << momd->mean_output() << ","
             << momd->variance_output() << std::endl;

      delete derd, momd;
    }

    myfile.close();
  } catch (std::exception &e) {
    std::cerr << "Error in Dense: " << e.what() << "\n";
  }

  // Sparse

  return 0;
}
