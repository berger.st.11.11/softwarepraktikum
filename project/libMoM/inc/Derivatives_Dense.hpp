#pragma once

#include <vector>

#include "Derivatives.hpp"
#include "Eigen/Dense"
#include "Function.hpp"
#include "dco.hpp"

namespace MoM {

///Struct zur Berechnung der Ableitungen
template <unsigned int n>
struct deriv {
  ///rekursive genutzte Funktion zur Berechnung der Ableitungen, kriegt Ableitungsrichtung übergeben
  template <typename T, typename VT>
  static void dern(const VT &, T &, std::vector<int>);
};

/// Basisfall n = 0
template <>
struct deriv<0> {
  template <typename T, typename VT>
  static void dern(const VT &, T &, std::vector<int>);
};

/// Klasse fuer Ableitung
template <typename T, typename VT>
class Derivatives_Dense : public Derivatives<T, VT> {
 protected:
  /// fuer direction bestimmung
  std::vector<std::vector<int>> v;
   /// fuer direction bestimmung
  std::vector<int> parts;

  /// directions
  void partitions(int, int);
  ///directions
  void direction_calculate(const int, std::vector<std::vector<int>> &);

 public:
 ///Konstruktor
  Derivatives_Dense(const int, const VT &);
///linearen Tensor mit Ableitungen fuellen (dense)
  void fill_tensor_lin();
};

}  // namespace MoM

#include "../src/Derivatives_Dense.cpp"