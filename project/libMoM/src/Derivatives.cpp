namespace MoM {

template <typename T, typename VT>
Derivatives<T, VT>::Derivatives(const int order, const VT &mean)
    : _order(order), _mean(mean) {
  _num_var = mean.size();
  _lin_der_tensor = std::vector<T>(num_permutations(_num_var, _order), 0);
}

template <typename T, typename VT>
const std::vector<T> &Derivatives<T, VT>::lin_der_tensor() {
  return _lin_der_tensor;
}

template <typename T, typename VT>
const int &Derivatives<T, VT>::order() {
  return _order;
}

template <typename T, typename VT>
int Derivatives<T, VT>::num_permutations(int num_var, int order) {
  if (num_var == 1) return order + 1;
  int num_perm = 0;
  for (int i = 0; i <= order; ++i) {
    num_perm += num_permutations(num_var - 1, i);
  }
  return num_perm;
}

}  // namespace MoM
