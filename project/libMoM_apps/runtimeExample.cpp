#include <Eigen/Dense>
#include <chrono>
#include <fstream>
#include <iostream>

#include "MoM.hpp"

// Funktion fuer die die Momentenmethode ausgefuehrt wird
template <typename T, typename VT>
void MoM::f(const VT &x, T &y) {
  y = 0;
  for (size_t i = 0; i < x.size(); ++i) {
    y *= x[i] * x[i] * x[i];
  }
}

int main(int argc, char *argv[]) {
  // Setup
  int order = 3;

  using T = double;
  using VT = Eigen::Matrix<double, Eigen::Dynamic, 1>;

  // VT mean_input = VT::Random(num_var, 1) * 2.75;
  // VT std_dev = VT::Random(num_var, 1) * 1.5;

  // Dense

  try {
    std::ofstream myfile;
    myfile.open("3Dplot.csv");
    int max_x = 12;
    int max_order = 6;
    for (int i = 1; i < max_x; i++) {
      for (int j = 1; j < max_order; j++) {
        myfile << i << "," << j << ",";
        VT mean_input = VT::Random(i, 1) * 2.75;
        VT std_dev = VT::Random(i, 1) * 1.5;

        auto t1 = std::chrono::high_resolution_clock::now();

        MoM::Derivatives_Dense<T, VT> *derd =
            new MoM::Derivatives_Dense<T, VT>(j, mean_input);
        MoM::MethodOfMoments<T, VT> *momd =
            new MoM::MethodOfMoments<T, VT>(std_dev, *derd);
        momd->calc_mean();
        momd->calc_variance();

        auto t2 = std::chrono::high_resolution_clock::now();
        auto duration =
            std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1)
                .count();
        std::cout << "Time MoM Dense: \033[31m" << duration / 1E6 << "s\033[0m"
                  << std::endl;

        std::cout << "F_mean_dense      = " << momd->mean_output() << std::endl;
        std::cout << "F_variance_dense  = " << momd->variance_output()
                  << std::endl;
        std::cout << std::endl;
        myfile << duration / 1E6 << std::endl;

        delete derd, momd;
      }
    }
    myfile.close();
  } catch (std::exception &e) {
    std::cerr << "Error in Dense: " << e.what() << "\n";
  }

  // Sparse
  /*
  try
  {
      std::ofstream myfile;
      myfile.open("DenseExampleSparseDerive100.csv");
      int max_x = 50;
      for (int i = 1; i < max_x; i++)
      {
          VT mean_input = VT::Random(i, 1) * 2.75;
          VT std_dev = VT::Random(i, 1) * 1.5;

          auto t1 = std::chrono::high_resolution_clock::now();

          MoM::Derivatives_Sparse<T, VT> *derd =
              new MoM::Derivatives_Sparse<T, VT>(order, mean_input);
          MoM::MethodOfMoments<T, VT> *momd =
              new MoM::MethodOfMoments<T, VT>(std_dev, *derd);
          momd->calc_mean();
          momd->calc_variance();

          auto t2 = std::chrono::high_resolution_clock::now();
          auto duration =
              std::chrono::duration_cast<std::chrono::microseconds>(t2 -
  t1).count(); std::cout << "Time MoM Dense: \033[31m" << duration / 1E6 <<
  "s\033[0m"
                    << std::endl;

          std::cout << "F_mean_dense      = " << momd->mean_output() <<
  std::endl; std::cout << "F_variance_dense  = " << momd->variance_output() <<
  std::endl; std::cout << std::endl; myfile << i << "," << duration / 1E6 <<
  std::endl;

          delete derd, momd;
      }
      myfile.close();
  }
  catch (std::exception &e)
  {
      std::cerr << "Error in Dense: " << e.what() << "\n";
  }
  */
  return 0;
}
